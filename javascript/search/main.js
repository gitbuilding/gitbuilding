import { createApp } from "petite-vue";
import { Fzf, byStartAsc } from "fzf";

createApp({
  searchTerm: getSessionSearchTerm(),
  searchResults: [],
  index: null,
  exactIndex: null,
  focused: false,
  focusTimeout: null,
  rootPath: "./",
  mounted(root) {
    this.rootPath = root;
    fetch(root + "search_index.json")
      .then((r) => r.json())
      .then((searchIndexJson) => {
        searchIndexJson = searchIndexJson.map((doc) => ({
          ...doc,
          breadCrumbs: getBreadCrumbs(doc.path, searchIndexJson),
        }));
        this.index = new Fzf(searchIndexJson, {
          limit: 10,
          tiebreakers: [byStartAsc],
          selector: (doc) => doc.content,
        });
        this.exactIndex = new Fzf(searchIndexJson, {
          limit: 10,
          tiebreakers: [byStartAsc],
          selector: (doc) => doc.content,
          fuzzy: false,
        });
        const term = getSessionSearchTerm();
        this.search(term);
      });
    // can't seem to get this from v-on:keypress on the input element
    document.addEventListener("keydown", (event) => {
      if (event.key === "Escape") {
        document.querySelector("#nav-search input").blur();
        this.focused = false;
      }
    });
  },
  search(term) {
    setSessionSearchTerm(term);
    this.searchTerm = term;
    if (term && this.index) {
      let results = find(this.index, term);
      if (results.length === 0) {
        results = find(this.exactIndex, term);
      }
      this.searchResults = results;
    }
  },
  handleKeyPress(event) {
    if (event.key === "Enter") {
      event.target.blur();
      this.focused = false;
      if (this.searchResults.length > 0) {
        window.location = this.rootPath + this.searchResults[0].path + ".html";
      }
    }
  },
  handleFocus() {
    clearTimeout(this.focusTimeout);
    this.focused = true;
  },
  handleBlur() {
    // remove focus after a delay so the user has time to click on links in the
    // popover
    clearTimeout(this.focusTimeout);
    this.focusTimeout = setTimeout(() => {
      this.focused = false;
    }, 200);
  },
  $delimiters: ["${", "}"],
}).mount("#nav-search");

function find(index, term) {
  // runs the search on the given index and computes highlight areas in the content
  let results = index.find(term);
  results = results.map((r) => ({
    ...r.item,
    highlightChars: computeHighlightChars(r.item.content, r.positions),
  }));
  // get rid results that only have matches of less than 3 characters
  // (unless search term is also less than 3 characters)
  results = results.filter((r) => {
    for (const node of r.highlightChars) {
      if (node.mark && node.chars.length >= Math.min(3, term.length)) {
        return true;
      }
    }
  });
  // get rid of highlights that are less than 3 characters (unless the
  // search term is also less than 3), they don't seem useful to the user
  results = results.map((r) => {
    return {
      ...r,
      highlightChars: r.highlightChars.map((node) =>
        node.mark && node.chars.length < Math.min(3, term.length)
          ? { ...node, mark: false }
          : node
      ),
    };
  });
  return results;
}

function getSessionSearchTerm() {
  return sessionStorage.getItem(`searchTerm`) || "";
}

function setSessionSearchTerm(term) {
  setTimeout(() => {
    sessionStorage.setItem(`searchTerm`, term);
  }, 0);
}

const CONTEXT = 30;
function computeHighlightChars(content, positions) {
  // seperates content into "nodes" of highlighted (`mark: true`) and
  // non-highlighted (`mark: false`) content. removes non-highlighted content
  // outside of the `CONTEXT` range and adds ellipses ("...")
  const chars = content.split("");

  const nodes = chars.reduce((prev, cur, i) => {
    const mark = positions.has(i);
    if (prev.length === 0) {
      return [{ mark, chars: cur }];
    }
    const last = prev[prev.length - 1];
    if (last.mark === mark) {
      last.chars += cur;
      return prev;
    }
    return prev.concat([{ mark, chars: cur }]);
  }, []);

  return nodes.map((node, i) => {
    if (node.mark) {
      return node;
    }
    if (node.chars.length <= CONTEXT * 2) {
      return node;
    }
    const last = nodes[i - 1];
    const next = nodes[i + 1];
    if (!last) {
      return { ...node, chars: "..." + node.chars.slice(-CONTEXT) };
    }
    if (!next) {
      return {
        ...node,
        chars: node.chars.slice(0, CONTEXT) + "...",
      };
    }
    return {
      ...node,
      chars: node.chars.slice(0, CONTEXT) + "..." + node.chars.slice(-CONTEXT),
    };
  }, []);
}

function getBreadCrumbs(filepath, index) {
  // returns arrays like `['Project Page', 'project_sub_page']`. determined from
  // the folder structure and titles looked up in the search index
  const split = splitPaths(filepath);
  for (const doc of index) {
    if (doc.path === split[0] && doc.title != null) {
      split[0] = doc.title;
      return split;
    }
  }
  return split;
}

function splitPaths(filepath) {
  if (filepath.endsWith("_BOM")) {
    return filepath.split(/_BOM$/)[0].split("/").concat(["BOM"]);
  }
  return filepath.split("/");
}
