import nodeResolve from 'rollup-plugin-node-resolve';
import image from '@rollup/plugin-image';

export default {
  input: 'main.js',
  plugins: [nodeResolve(), image()],
  output: {
    format: 'umd',
    file: '../../gitbuilding/static/3d-viewer.js',
  },
};
