#!/bin/bash

cd javascript/3d-viewer
npm install
npm run build
cd ../webapp
npm install
npm run lint
npm run build
cd ../search
npm install
npm run build
