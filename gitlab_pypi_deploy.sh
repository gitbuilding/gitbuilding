#! /bin/bash

DATE_FMT=$(date +"%Y%m%d")
sed -r -i "s/(^__version__ = '[0-9]+.[0-9]+.[0-9]+.dev)(0('$))/\1$DATE_FMT\3/g" setup.py
python setup.py sdist bdist_wheel
